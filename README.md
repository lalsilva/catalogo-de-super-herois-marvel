# MarvelComics

Este projeto foi gerado com [Angular CLI](https://github.com/angular/angular-cli) versão 10.1.7.

## Servidor de desenvolvimento

Execute `ng serve` para um servidor de desenvolvimento. Navegue pela URL `http://localhost:4200/`. O aplicativo será recarregado automaticamente se você alterar qualquer um dos arquivos de origem.

## Estrutura de código

Execute `ng generate component component-name` para gerar um novo componente. Você também pode usar `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Buildar

Execute `ng build` para buildar o projeto. Os artefatos de construção serão armazenados no diretório `dist/`. Use o sinalizador `--prod` para buildar para produção.

## Executando testes unitários

Execute `ng test` para executar os testes de unidade via [Karma](https://karma-runner.github.io).

## Execução de testes ponta a ponta (end-to-end)

Execute `ng e2e` para executar os testes ponta a ponta via [Protractor](http://www.protractortest.org/).

## Mais ajuda

Para obter mais ajuda sobre o Angular CLI use `ng help` ou verifique o [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
