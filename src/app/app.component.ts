import { Component } from '@angular/core';

@Component({
  selector: 'marvel-comics-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'marvel-comics';
}
