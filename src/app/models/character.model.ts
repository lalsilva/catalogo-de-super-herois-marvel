import { Image } from './image.model';

export interface Character {
    id: number;
    name: string;
    description: string;
    thumbnail: Image;
    modified: string;
}
