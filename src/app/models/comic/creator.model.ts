export interface ComicCreator {
    name: string;
    resourceURI: string;
    role: string;
}
