import { ComicCreator } from './creator.model';

export interface ComicCreators {
    available: number;
    collectionURI: string;
    items: ComicCreator[];
    returned: number;
}
