import { Image } from './image.model';
import { ComicCreators } from './comic/creators.model';

export interface Comic {
    id: number;
    digitalId: number;
    title: string;
    description: string;
    thumbnail: Image;
    images: Image[];
    dates: any[];
    creators: ComicCreators[];
}
