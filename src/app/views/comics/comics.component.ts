import { Component, OnInit } from '@angular/core';
import { catchError, map } from 'rxjs/operators';

import { PageEvent } from '@angular/material/paginator';

import { ComicsService } from 'src/app/services/comics.service';
import { ErrorService } from 'src/app/services/error.service';
import { Comic } from 'src/app/models/comic.model';

@Component({
  selector: 'marvel-comics-comics',
  templateUrl: './comics.component.html',
  styleUrls: ['./comics.component.scss']
})
export class ComicsComponent implements OnInit {

  comics: Comic[];

  lengthOfComics: number;
  pageEvent: PageEvent;

  isLoaded: boolean = false;

  constructor(
    private comicService: ComicsService,
    private errorService: ErrorService
  ) { }

  ngOnInit(): void {
    this.getListOfComics();
  }

  getListOfComics(e?: PageEvent): PageEvent {
    let offset = e ? e.pageIndex * e.pageSize : 0;

    this.isLoaded = false;

    this.comicService.read(offset).pipe(
      map((response) => {
        this.comics = response.data.results;
        this.lengthOfComics = response.data.total;
        this.isLoaded = true;
      }),
      catchError((e) => this.errorService.errorHandler(e))
    ).subscribe();

    return e;
  }

}
