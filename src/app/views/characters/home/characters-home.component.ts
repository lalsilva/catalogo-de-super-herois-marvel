import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { catchError, debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';

import { PageEvent } from '@angular/material/paginator';

import { CharactersService } from 'src/app/services/characters.service';
import { ErrorService } from 'src/app/services/error.service';
import { Character } from 'src/app/models/character.model';
import { fromEvent } from 'rxjs';

@Component({
  selector: 'marvel-comics-characters',
  templateUrl: './characters-home.component.html',
  styleUrls: ['./characters-home.component.scss']
})
export class CharactersHomeComponent implements OnInit, AfterViewInit {

  characters: Character[];

  lengthOfCharacters: number;
  pageEvent: PageEvent;

  isLoaded: boolean = false;

  @ViewChild('input', { static: true }) input: ElementRef;
  @ViewChild('searchMessage', { static: true }) searchMessage: ElementRef;

  constructor(
    private charactersService: CharactersService,
    private errorService: ErrorService
  ) { }

  ngOnInit(): void {
    this.getListOfCharacters();
  }

  ngAfterViewInit() {
    fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        filter(Boolean),
        debounceTime(1500),
        distinctUntilChanged(),
        map((event: KeyboardEvent) => {
          this.filterCharacters(this.input.nativeElement.value);
        })
      ).subscribe();
  }

  getListOfCharacters(e?: PageEvent): PageEvent {
    let offset = e ? e.pageIndex * e.pageSize : 0;

    this.isLoaded = false;

    this.charactersService.read(offset).pipe(
      map((response) => {
        this.characters = response.data.results;
        this.lengthOfCharacters = response.data.total;
        this.isLoaded = true;
      }),
      catchError((e) => this.errorService.errorHandler(e))
    ).subscribe();

    return e;
  }

  filterCharacters(expression: string, nameStartsWith?: boolean): void {
    if (expression.length > 2 || nameStartsWith) {
      this.isLoaded = false;

      this.charactersService.filter(expression, nameStartsWith).pipe(
        map((response) => {
          this.characters = response.data.results;
          this.lengthOfCharacters = response.data.total;
          this.isLoaded = true;

          if (this.lengthOfCharacters === 0) {
            this.searchMessage.nativeElement.innerHTML = 'Não foi encontrado nenhum personagem!';
          }
        }),
        catchError((e) => this.errorService.errorHandler(e))
      ).subscribe();
    } else if (expression.length == 0) {
      this.getListOfCharacters();
    }
  }

  onSearchByLetter(letter: string): void {
    if (letter.length > 0) {
      this.filterCharacters(letter, true);
    } else {
      this.filterCharacters('');
    }
  }
}
