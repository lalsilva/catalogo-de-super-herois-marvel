import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { catchError, map } from 'rxjs/operators';

import { Character } from 'src/app/models/character.model';
import { CharactersService } from 'src/app/services/characters.service';
import { ErrorService } from 'src/app/services/error.service';
import { FavoriteCharacterService } from 'src/app/services/favorite-character.service';

@Component({
  selector: 'marvel-comics-favorites',
  templateUrl: './characters-favorites.component.html',
  styleUrls: ['./characters-favorites.component.scss']
})
export class CharactersFavoritesComponent implements OnInit {

  characters: Character[] = [];

  lengthOfCharacters: number;
  lengthOfErrorsLoading: number = 0;

  isLoaded: boolean = false;

  messageNoFavorites: string = 'Não existe(m) personagem(ns) favorito(s)!';

  @ViewChild('favoriteMessage', { static: true }) favoriteMessage: ElementRef;

  constructor(
    private charactersService: CharactersService,
    private errorService: ErrorService,
    private favoritesService: FavoriteCharacterService
  ) { }

  ngOnInit(): void {
    this.getListOfCharacters();
  }

  getListOfCharacters(): void {
    let listOfCharactersFavorited: any[] = this.favoritesService.getFromStorage() || [];

    for (let i: number = 0; i < listOfCharactersFavorited.length; i++) {
      let id: number = listOfCharactersFavorited[i].characterId;

      this.charactersService.readById(id).pipe(
        map((response) => {
          if (response.code === 200 && response.status === 'Ok') {
            this.characters.push(response.data.results[0]);
          } else {
            this.lengthOfErrorsLoading++;
          }

          this.lengthOfCharacters = this.characters.length + this.lengthOfErrorsLoading;

          if (this.lengthOfCharacters === listOfCharactersFavorited.length) {
            this.characters.sort((a, b) => a.name.localeCompare(b.name));
            this.isLoaded = true;
          }
        }),
        catchError((e) => this.errorService.errorHandler(e))
      ).subscribe();
    }

    if (listOfCharactersFavorited.length === 0) {
      this.favoriteMessage.nativeElement.innerHTML = this.messageNoFavorites;
      this.isLoaded = true;
    }
  }

  onClearFavoritesClick(e) {
    this.favoriteMessage.nativeElement.innerHTML = this.messageNoFavorites;
    this.characters = [];
    this.lengthOfCharacters = 0;
    this.lengthOfErrorsLoading = 0;
    this.favoritesService.clearFavorites();
  }

}
