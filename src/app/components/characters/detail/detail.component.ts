import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { catchError, map } from 'rxjs/operators';

import { Character } from 'src/app/models/character.model';
import { CharactersService } from 'src/app/services/characters.service';
import { ErrorService } from 'src/app/services/error.service';
import { FavoriteCharacterService } from 'src/app/services/favorite-character.service';

@Component({
  selector: 'marvel-comics-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class CharactersDetailComponent implements OnInit {

  character: Character;

  private _thumbnail: string;
  private _dateModified: string;

  isLoaded: boolean = false;

  iconFavorite: string = 'favorite_border';

  constructor(
    private route: ActivatedRoute,
    private characterService: CharactersService,
    private errorService: ErrorService,
    private favoriteService: FavoriteCharacterService
  ) { }

  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get('id');

    let isFavorited: any = this.favoriteService.isFavorited(id);

    if (isFavorited) {
      this.iconFavorite = 'favorite';
    } else {
      this.iconFavorite = 'favorite_border';
    }

    this.characterService.readById(id).pipe(
      map((response) => {
        this.character = response.data.results[0];
        this.thumbnail = this.character.thumbnail;
        this.dateModified = this.character.modified;
        this.isLoaded = true;
      }),
      catchError((e) => this.errorService.errorHandler(e))
    ).subscribe();
  }

  set thumbnail(thumbnail: any) {
    let url: string;

    if (thumbnail.path.indexOf('image_not_available') > -1) {
      url = 'assets/images/card/no-image.png';
    } else {
      url = `${thumbnail.path}.${thumbnail.extension}`;
    }

    this._thumbnail = url;
  }

  get thumbnail(): any {
    return this._thumbnail;
  }

  set dateModified(date: string) {
    this._dateModified = date;
  }

  get dateModified(): string {
    return this._dateModified;
  }

  onFavoriteClick(id: number) {
    let isFavorited: any = this.favoriteService.isFavorited(id);

    if (!isFavorited) {
      this.favoriteService.addToStorage({ characterId: id });
      this.iconFavorite = 'favorite';
    } else {
      this.favoriteService.removeFromStorage(id);
      this.iconFavorite = 'favorite_border';
    }
  }

}
