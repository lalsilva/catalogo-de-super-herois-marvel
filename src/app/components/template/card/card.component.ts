import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'marvel-comics-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() type: string;
  @Input() route: string;
  @Input() items: string[];

  constructor() { }

  ngOnInit(): void {
  }

  getThumbnail(item: any): string {
    let url: string;

    if (item.thumbnail.path.indexOf('image_not_available') > -1) {
      url = `assets/images/card/no-image.png`;
    } else {
      url = `${item.thumbnail.path}.${item.thumbnail.extension}`;
    }

    return url;
  }

  getTitle(item: any): string {
    let title: string;

    if (this.type === 'comic') {
      title = item.title;
    } else if (this.type === 'character') {
      title = item.name;
    }

    return title;
  }

  onMouseEnter(e): void {
    let background = e.currentTarget.querySelector('div.background');
    background.style.height = '100%';
  }

  onMouseLeave(e): void {
    let background = e.currentTarget.querySelector('div.background');
    background.style.height = 0;
  }

}
