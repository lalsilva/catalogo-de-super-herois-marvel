import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { catchError, map } from 'rxjs/operators';

import { Comic } from 'src/app/models/comic.model';
import { ComicCreator } from 'src/app/models/comic/creator.model';
import { ComicsService } from 'src/app/services/comics.service';
import { ErrorService } from 'src/app/services/error.service';

@Component({
  selector: 'marvel-comics-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class ComicsDetailComponent implements OnInit {

  comic: Comic;

  private _thumbnail: string;
  private _dateOnSale: string;
  private _creators: ComicCreator[];

  isLoaded: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private comicService: ComicsService,
    private errorService: ErrorService
  ) { }

  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.comicService.readById(id).pipe(
      map((response) => {
        this.comic = response.data.results[0];
        this.thumbnail = this.comic.thumbnail;
        this.dateOnSale = this.comic.dates;
        this.creators = this.comic.creators;
        this.isLoaded = true;
      }),
      catchError((e) => this.errorService.errorHandler(e))
    ).subscribe();
  }

  set thumbnail(thumbnail: any) {
    let url: string;

    if (thumbnail.path.indexOf('image_not_available') > -1) {
      url = 'assets/images/card/no-image.png';
    } else {
      url = `${thumbnail.path}.${thumbnail.extension}`;
    }

    this._thumbnail = url;
  }

  get thumbnail(): any {
    return this._thumbnail;
  }

  set dateOnSale(dates: any) {
    let date = dates.filter(date => date.type === 'onsaleDate')[0].date;
    this._dateOnSale = date;
  }

  get dateOnSale(): any {
    return this._dateOnSale;
  }

  set creators(creators: any) {
    let listOfCreators: ComicCreator[] = [];

    for (let creator of creators['items']) {
      listOfCreators.push(creator);
    }

    this._creators = listOfCreators;
  }

  get creators(): any {
    return this._creators;
  }
}
