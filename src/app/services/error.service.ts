import { Injectable } from '@angular/core';
import { EMPTY, Observable } from 'rxjs';

import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor(
    private snackBar: MatSnackBar
  ) { }

  showMessage(msg: string): void {
    this.snackBar.open(msg, '', {
      duration: 3000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
      panelClass: 'msg-error'
    });
  }

  errorHandler(e: any): Observable<any> {
    console.error(e);
    this.showMessage('Ocorreu um erro!');
    return EMPTY;
  }
}
