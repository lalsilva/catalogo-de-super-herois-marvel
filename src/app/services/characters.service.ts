import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class CharactersService {

  baseUrl: string = 'http://gateway.marvel.com/v1/public/characters';

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) { }

  read(offset: number = 0): Observable<any> {
    const url = `${this.baseUrl}?apikey=${this.authService.apiKey}&hash=${this.authService.hash}&ts=${this.authService.ts}&offset=${offset}`;
    return this.http.get<any>(url);
  }

  readById(id: number): Observable<any> {
    const url = `${this.baseUrl}/${id}?apikey=${this.authService.apiKey}&hash=${this.authService.hash}&ts=${this.authService.ts}`;
    return this.http.get<any>(url);
  }

  filter(expression: string, nameStartsWith?: boolean): Observable<any> {
    let paramFilter: string;

    if (nameStartsWith) {
      paramFilter = `&nameStartsWith=${expression}`;
    } else {
      paramFilter = `&name=${expression}`;
    }

    const url = `${this.baseUrl}?apikey=${this.authService.apiKey}&hash=${this.authService.hash}&ts=${this.authService.ts}${paramFilter}`;

    return this.http.get<any>(url);
  }

}
