import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FavoriteCharacterService {

  constructor() { }

  getFromStorage(): any[] {
    return JSON.parse(localStorage.getItem('favoriteMarvelCharacters'));
  }

  isFavorited(id: number): any {
    let listOfFavorites: any[] = JSON.parse(localStorage.getItem('favoriteMarvelCharacters'));

    if (listOfFavorites) {
      return listOfFavorites.find((element) => element.characterId == id);
    } else {
      return false;
    }
  }

  addToStorage(data: any): void {
    let listOfFavorites: any[] = this.getFromStorage() || [];

    data.date = new Date();

    listOfFavorites.push(data);

    localStorage.setItem('favoriteMarvelCharacters', JSON.stringify(listOfFavorites));
  }

  removeFromStorage(id: number): void {
    let listOfFavorites: any[] = this.getFromStorage();
    let strOfFavorites: string = JSON.stringify(listOfFavorites);

    if (this.isFavorited(id)) {
      strOfFavorites = strOfFavorites.replace(new RegExp(`(,?{\"[a-zA-Z]*\":${id})(,\"*[a-z]*[\":A-Z0-9*\-.]*}),?`), '');

      localStorage.setItem('favoriteMarvelCharacters', strOfFavorites);
    }
  }

  clearFavorites(): void {
    localStorage.setItem('favoriteMarvelCharacters', null);
  }

}
