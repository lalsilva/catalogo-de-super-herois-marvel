import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _apiKey: string = '1bade5ac122ca8e08b251d4f8331834e';
  private _hash: string = 'fd32479b9dddb71c3a3162d04d8664e3';
  private _ts: number = 1;

  constructor() { }

  get apiKey(): string {
    return this._apiKey;
  }

  get hash(): string {
    return this._hash;
  }

  get ts(): number {
    return this._ts;
  }

}
