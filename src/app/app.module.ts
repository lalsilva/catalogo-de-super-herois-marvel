import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { LOCALE_ID, NgModule } from '@angular/core';
import localePt from '@angular/common/locales/pt';
import { registerLocaleData } from '@angular/common';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatCardModule } from '@angular/material/card';
import { MatPaginatorIntl, MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/template/header/header.component';
import { HomeComponent } from './views/home/home.component';
import { ComicsComponent } from './views/comics/comics.component';
import { ComicsDetailComponent } from './components/comics/detail/detail.component';
import { CharactersHomeComponent } from './views/characters/home/characters-home.component';
import { CharactersDetailComponent } from './components/characters/detail/detail.component';
import { CardComponent } from './components/template/card/card.component';
import { CustomPaginator } from './utils/custom-paginator';
import { CharactersFavoritesComponent } from './views/characters/favorites/characters-favorites.component';

registerLocaleData(localePt);

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    ComicsComponent,
    ComicsDetailComponent,
    CharactersHomeComponent,
    CharactersDetailComponent,
    CardComponent,
    CharactersFavoritesComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatListModule,
    MatSnackBarModule,
    MatCardModule,
    MatPaginatorModule,
    MatProgressBarModule
  ],
  providers: [{
    provide: LOCALE_ID,
    useValue: 'pt-BR'
  }, {
    provide: MatPaginatorIntl,
    useClass: CustomPaginator
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
