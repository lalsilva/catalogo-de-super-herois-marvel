import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './views/home/home.component';
import { ComicsComponent } from './views/comics/comics.component';
import { ComicsDetailComponent } from './components/comics/detail/detail.component';
import { CharactersHomeComponent } from './views/characters/home/characters-home.component';
import { CharactersFavoritesComponent } from './views/characters/favorites/characters-favorites.component';
import { CharactersDetailComponent } from './components/characters/detail/detail.component';

const routes: Routes = [
  { path: 'comics', component: ComicsComponent },
  { path: 'comics/:id', component: ComicsDetailComponent },
  { path: 'characters', component: CharactersHomeComponent },
  { path: 'characters/favorites', component: CharactersFavoritesComponent },
  { path: 'characters/:id', component: CharactersDetailComponent },
  { path: '', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
